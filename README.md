gitlab page https://gitlab.com/userAA/six-simple-projects-english-comments.git
gitlab comment six-simple-projects-english-comments

a. project my-app1 (counter).
technologies used
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-dom,
    react-scripts,
    sass.

b. project my-app2 (modal window).
technologies used
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-dom,
    react-scripts,
    sass.

c. project my-app3 (quiz).
technologies used
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-dom,
    react-scripts,
    sass.

d. project my-app4 (list of users).
technologies used
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-content-loader,
    react-dom,
    react-scripts,
    sass.

e. project my-app5 (currency converter).
technologies used
    @emotion/react,
    @emotion/styled,
    @mui/icons-material,
    @mui/material,
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    axios,
    framer-motion,
    react,
    react-dom,
    react-scripts,
    sass.

f. project my-app6 (photo collections).
technologies used
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-dom,
    react-scripts,
    sass.