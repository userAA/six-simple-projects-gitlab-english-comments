import React from 'react';

const defaultCurrencies = ['RUB', 'USD', 'EUR', 'GBP'];

//currencies block of dependent or dependent subordinate
export const Block = ({
    //the quantity of dependent or subordinate currency
    value, 
    //the name of dependent or subordinate currency
    currency, 
    //the changing name function of dependent or subordinate currency
    onChangeValue, 
    //the function of changing of subordinate currency quantity in relation to dependent currency quantity or
    //the function of changing of dependent currency quantity in relation to subordinate currency quantity
    onChangeCurrency
}) => (
    <div className="block">
        <ul className="currencies">
            {defaultCurrencies.map((cur) => (
                //предоставляем возможность изменения названия зависящей или зависимой валюты
                //we provide the possibility to change the name of dependent or subordinate currency
                <li
                    onClick={() => onChangeCurrency(cur)}
                    className={currency === cur ? 'active' : ''}
                    key={cur}
                >
                    {cur}
                </li>
            ))}
            <li>
                <svg height="50px" viewBox="0 0 50 50" width="50px">
                    <rect fill="none" height="50" width="50"/>
                    <polygon points="47.25,15 45.164,12.914 25,33.078 4.836,12.914 2.75,15 25,37.25 " />
                </svg>
            </li>
        </ul>
        {/*we provide the possibility to change the amount of dependent or subordinate currency*/}
        <input
            onChange={(e) => onChangeValue(e.target.value)}
            value={value}
            type="number"
            placeholder={0}
        />
    </div>
)