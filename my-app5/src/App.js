import React from 'react';
import {Block} from './Block';
import './index.scss';

//cryptocurrency page
function App() {
  //name of the dependent currency
  const [fromCurrency, setFromCurrency] = React.useState('RUB');
  //name of the subordinate currency
  const [toCurrency, setToCurrency] = React.useState('USD');
  //the state of dependent currency quantity
  const [fromPrice, setFromPrice] = React.useState(0);
  //the state of subordinate currency quantity
  const [toPrice, setToPrice] = React.useState(1);

  //the state of the set of currency values in dollars based on their names
  const ratesRef = React.useRef({});

  //the function of changing the amount of subordinate currency in relation to the amount of dependent currency
  const onChangeFromPrice = (value) => 
  {
    //we count the amount of the dependent currency in dollars
    const price = value / ratesRef.current[fromCurrency];
    //we count the amount of the subordinate currency by the source currency
    const result = price * ratesRef.current[toCurrency];
    //we fixing ammount of subordinate currency
    setToPrice(result.toFixed(3));
    //we fixing ammount of dependent currency
    setFromPrice(value);
  }

  //the function of changing the amount of dependent currency in relation to the amount of subordinate currency
  const onChangeToPrice = (value) => {
    const result = (ratesRef.current[fromCurrency] / ratesRef.current[toCurrency])*value; 
    //we fixing the amount of dependent currency according to subordinate
    setFromPrice(result.toFixed(3));
    //we fixing the quantity of subordinate currency 
    setToPrice(value);
  }

  React.useEffect(() => {
    fetch('https://cdn.cur.su/api/latest.json').then((res) => res.json()).then((json) => 
    {
      //we received a set of currencies values for different currencies based on the dollar
      ratesRef.current = json.rates;
      onChangeToPrice(1);
    })
    .catch((err) => {
      console.warn(err);
      alert('Failed to get information')
    })
  }, [onChangeToPrice])

  React.useEffect(() => {
    //we changing the amount of dependent currency according to subordinate 
    //depending on the name change of the dependent currency
    onChangeFromPrice(fromPrice);
  }, [onChangeFromPrice, fromPrice, fromCurrency]);

  React.useEffect(() => {
    //we changing the amount of subordinate currency according to dependent
    //depending on the name change of the subordinate currency
    onChangeToPrice(toPrice);
  }, [onChangeToPrice, toPrice, toCurrency]);

  return (
    <div className="App">
      {/*showing a block of dependent currencies */}
      <Block 
        //the quantity of dependent currency
        value={fromPrice} 
        //the name of dependent currency
        currency={fromCurrency} 
        //function of changing the name of the dependent currency
        onChangeCurrency={setFromCurrency} 
        //the function of changing the amount of subordinate currency in relation to the amount of dependent currency
        onChangeValue={onChangeFromPrice}
      />
      {/*showing a block of subordinate currencies */}
      <Block 
        //the quantity of subordinate currency
        value={toPrice}
        //the name of subordinate currency
        currency={toCurrency} 
        //function of changing the name of the subordinate currency
        onChangeCurrency={setToCurrency}
        //the function of changing the amount of dependent currency in relation to the amount of subordinate currency
        onChangeValue={onChangeToPrice}
      />   
    </div>
  );
}

export default App;