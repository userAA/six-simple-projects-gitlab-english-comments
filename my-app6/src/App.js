import React from 'react';
import {Collection} from './Collection';
import './index.scss';

//list of categories of photo collections
const cats = [
  { "name": "All" },
  { "name": "Sea" },
  { "name": "Mountains" },
  { "name": "Architecture" },
  { "name": "Cities" }
];

//the component of displaying collections of photos
function App() {
  //current category of photo collections
  const [categoryId,   setCategoryId] = React.useState(0);
  //photo collection display page
  const [page,               setPage] = React.useState(0);
  //downloading flag of photo collections from json-server
  const [isLoading,     setIsLoading] = React.useState(true);
  //text label for searching of a collection of photos among the downloaded and 
  //presented collections of photos on the page for a given category
  const [searchValue, setSearchValue] = React.useState('');
  //the list of photo collections displayed from the server, which is displayed on a separate page
  const [collections, setCollections] = React.useState([]);

  React.useEffect(() => {
    //we activate the downloading flag of photo collections from server
    setIsLoading(true);

    const category = categoryId ? `category=${categoryId}` : ``;

    //we downloading the known category of photo collections
    fetch(`http://localhost:3001/collections?${category}`)
    .then((res) => res.json())
    .then((json) => {
      //maximum of three collections of photos for the selected collection category
      const limit = 3;
      if (json.length < page*limit)
      {
        //there are no collections of photos according to needed category for the page with the page number
        setCollections([]);
      }
      else
      {
        //the required photo collections for the selected category of photo collections are collected in an array
        //paginationJson for page with the number page
        let paginationJson = [];
        let numElem = 1;
        {json.map((obj, i) => {
          if (i >= page*limit && numElem <=limit)
          {
            numElem++
            paginationJson.push(obj)
          }
        })}
        //we have identified collections of photos according to the desired category for the page with the page number
        setCollections(paginationJson);
      }
    })
    .catch((err) => {
      console.warn(err);
      alert('Error receiving data');
    })
    .finally(() => {
      //desired category of photo collections was downloaded
      setIsLoading(false)
    })
  }, [categoryId, page]);

  return (
    <div className="App">
      <h1>My Photo Collections</h1>
      <div className="top">
        <ul className="tags">
          {/*We representing all categories of photo collections  */}
          {cats.map((obj, i) => (
            <li 
              //we representing the posibility of current category changing of photo collections
              onClick={() => setCategoryId(i)} 
              className={categoryId === i ? 'active' : ''} 
              key={obj.name}
            >
              {/*Name of the photo collection category */}
              {obj.name}
            </li>
          ))}
        </ul>
        {/*The label for searching of the desired collection of photos according to its name */} 
        <input 
          value={searchValue}
          onChange={(e) => setSearchValue(e.target.value)}
          className="search-input" 
          placeholder="Search by name" 
        />
      </div>
      <div className="content">
        {isLoading ? (
          <h2>The download is in progress...</h2> 
        ) : (
          //we showing the downloaded photo collections 
          collections.filter(obj =>  obj.name.toLowerCase().includes(searchValue.toLowerCase()))
          .map((obj, index) => (
            <Collection key={index} name={obj.name} images={obj.photos} />
          ))
        )}
      </div>
      <ul className="pagination">
        {/*Photo collection display pages */}
        {[...Array(5)].map((_, i) => (
          //the photo collections display page can be active
          <li onClick={() => setPage(i)} className={page === i  ? 'active' : ''}>
            {/*page number of the display of the selected category of photo collections */}
            {i + 1}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default App;
