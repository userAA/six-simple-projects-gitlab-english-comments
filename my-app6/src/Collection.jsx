import React from 'react'

//photo collection display component
export const Collection = ({
    //all photos in the submitted collection
    images, 
    //name of the submitted photo collection
    name
}) => {
    return (
        <div className="collection">
            {/*Main image of submitted photo collection*/}
            <img className="collection__big" src={images[0]} alt="Item" />
            <div className="collection_bottom">
                {/*First small image of submitted photo collection*/}
                <img className="collection__mini" src={images[1]} alt="Item" />
                {/*Second small image of submitted photo collection*/}
                <img className="collection__mini" src={images[2]} alt="Item" />
                {/*Third small image of submitted photo collection*/}
                <img className="collection__mini" src={images[3]} alt="Item" />
            </div>
            <h4>{name}</h4>
        </div>
    )
}

export default Collection;