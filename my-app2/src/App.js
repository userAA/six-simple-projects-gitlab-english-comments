import React from 'react';
import './index.scss';

//modal window
const Modal = ({open, setOpen, children}) => {
  return (
    <div className={`overlay animated ${open ? 'show' : ''}`}>
      <div className="modal">
        {/*Closing modal window button */}
        <svg onClick={() => setOpen(false)} height="200" viewBox="0 0 200 200" width="200">
          <path d="M114,100l49-49a9.9,9.9,0,0,0-14-14L100,86,51,37A9.9,9.9,0,0,0,37,51l49,49L37,149a9.9,9.9,0,0,0,14,14l49-49,49,49a9.9,9.9,0,0,0,14-14Z" />
        </svg>  
        {/*The image in modal window */}
        {children}      
      </div>
    </div>
  )
}

//full function of opening a modal window
function App() {
  //the state of opening a modal window
  const [open, setOpen] = React.useState(false);

  return (
    <div className="App">
      {/*opening a modal window button*/}
      <button onClick={() => setOpen(true)} className="open-modal-btn">
        Open window
      </button>
      {/*Itself modal window with transmitted to it props*/}
      <Modal open={open} setOpen={setOpen}>
        {/*It is children in modal window*/}
        <img src="https://media2.giphy.com/media/xT0xeJpnrWC4XWblEk/giphy.gif" />
        <h3>This modal window</h3>  
        <button>BITCH IROCHKA</button>
      </Modal>
    </div>
  )
}

export default App;