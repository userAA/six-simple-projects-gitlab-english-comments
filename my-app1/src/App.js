import React from 'react';
import './index.scss';

//full counter function
function App() {
  const [count, setCount] = React.useState(0);

  //counter function in negative direction 
  const onClickMinus = () => {
    setCount(count-1);
  }

  //counter function in positive direction 
  const onClickPlus = () => {
    setCount(count+1);
  }

  return (
    <div className="App">
      <div>
        {/*The counter result itself */}
        <h2>Counter:</h2>
        <h1>{count}</h1>
        {/*Counter starting up button in negative direction */}
        <button onClick={onClickMinus} className="minus">- Minus</button>
        {/*Counter starting up button in positive direction */}
        <button onClick={onClickPlus} className="plus">Plus +</button>
      </div>
    </div>
  )
}

export default App;